/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default class Constants {
    public static SIZE_OF_BYTE: number = 1;
    public static SIZE_OF_SHORT: number= 2;
    public static SIZE_OF_INT: number= 4;
    public static SIZE_OF_LONG: number = 8;
    public static MAGIC_NUMBER: number = -47205080;
    public static MIN_WINDOW_LOG: number = 10;
    public static MAX_WINDOW_LOG: number = 31;
    public static SIZE_OF_BLOCK_HEADER: number = 3;
    public static MIN_SEQUENCES_SIZE: number = 1;
    public static MIN_BLOCK_SIZE: number = 1 + 1 + Constants.MIN_SEQUENCES_SIZE;
    public static MAX_BLOCK_SIZE: number = 128 * 1024;
    public static REPEATED_OFFSET_COUNT: number = 3;
    public static RAW_BLOCK: number = 0;
    public static RLE_BLOCK: number= 1;
    public static COMPRESSED_BLOCK: number = 2;
    public static SEQUENCE_ENCODING_BASIC: number = 0;
    public static SEQUENCE_ENCODING_RLE: number = 1;
    public static SEQUENCE_ENCODING_COMPRESSED: number = 2;
    public static SEQUENCE_ENCODING_REPEAT: number = 3;
    public static MAX_LITERALS_LENGTH_SYMBOL: number = 35;
    public static MAX_MATCH_LENGTH_SYMBOL: number = 52;
    public static MAX_OFFSET_CODE_SYMBOL: number = 31;
    public static DEFAULT_MAX_OFFSET_CODE_SYMBOL: number = 28;
    public static LITERAL_LENGTH_TABLE_LOG: number = 9;
    public static MATCH_LENGTH_TABLE_LOG: number= 9;
    public static OFFSET_TABLE_LOG: number= 8;
    public static RAW_LITERALS_BLOCK: number = 0;
    public static RLE_LITERALS_BLOCK: number= 1;
    public static COMPRESSED_LITERALS_BLOCK: number = 2;
    public static TREELESS_LITERALS_BLOCK: number= 3;
    public static LONG_NUMBER_OF_SEQUENCES: number = 32512;
    public static LITERALS_LENGTH_BITS: Int32Array= new Int32Array([
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 2, 2, 3, 3,
        4, 6, 7, 8, 9, 10, 11, 12,
        13, 14, 15, 16
    ]
    );
    public static MATCH_LENGTH_BITS: Int32Array = new Int32Array(
        [
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 2, 2, 3, 3,
            4, 4, 5, 7, 8, 9, 10, 11,
            12, 13, 14, 15, 16
        ]);

    constructor() {
    }
}
