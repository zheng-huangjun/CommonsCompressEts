/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { snappyCompress, snappyUncompress } from '@ohos/commons-compress';
import fileio from '@ohos.fileio';
import ability_featureAbility from '@ohos.ability.featureAbility'

@Entry
@Component
export struct SnappyTest {
  @State newfolder: string = 'newfolder'
  @State newfile: string = 'bla.txt'
  @State newfile1: string = 'bla1.txt'
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('Snappy相关功能')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)

      Button('bla.txt')
        .backgroundColor(0x2788D9)
        .margin(10)
        .width(150)
        .onClick(() => {
          if (!this.isFastClick()) {
            this.createFile()
          }
        })

      Button('compressed')
        .backgroundColor(0x2788D9)
        .margin(10)
        .width(150)
        .onClick(() => {
          if (!this.isFastClick()) {
            this.snappyJsTest(true)
          }
        })

      Button('uncompressed')
        .backgroundColor(0x2788D9)
        .margin(10)
        .width(150)
        .onClick(() => {
          if (!this.isFastClick()) {
            this.snappyJsTest(false)
          }
        })
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    this.createFolder()
  }

  createFolder() {
    var context = ability_featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        try {
          fileio.mkdirSync(data + '/' + this.newfolder)
        } catch (err) {
        }
      })
      .catch((error) => {
        console.error('Failed to obtain the cache directory. Cause:' + error.message);
      })
  }

  createFile() {
    var context = ability_featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        let fd = fileio.openSync(data + '/' + this.newfolder + '/' + this.newfile, 0o102, 0o666);
        fileio.writeSync(fd, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
        );
        fileio.closeSync(fd);
        AlertDialog.show({ title: '生成成功',
          message: '请查看手机路径' + data + '/' + this.newfolder + '/' + this.newfile,
          confirm: { value: 'OK', action: () => {
          } }
        })
      })
      .catch((error) => {
        console.error('Failed to obtain the cache directory. Cause:' + error.message);
      })
  }

  snappyJsTest(value) {
    console.log('snappyCompress');
    var context = ability_featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        if (value) {
          let path = data + '/' + this.newfolder
          console.log('snappyCompress');
          snappyCompress(path, this.newfile)
            .then(() => {
              AlertDialog.show({ title: '压缩成功',
                message: '请查看手机路径 ' + data + '/' + this.newfolder + '/' + this.newfile + '.sz',
                confirm: { value: 'OK', action: () => {
                } }
              })
            });
        } else {
          console.log('snappyUncompress');
          snappyUncompress(data, this.newfolder, this.newfile, this.newfile1)
            .then(() => {
              AlertDialog.show({ title: '解缩成功',
                message: '请查看手机路径 ' + data + '/' + this.newfile1,
                confirm: { value: 'OK', action: () => {
                } }
              })
            });
        }
      })
      .catch((error) => {
        console.error('Failed to obtain the file directory. Cause: ' + error.message);
      })
  }

  isFastClick(): boolean {
    var timestamp = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}
