/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "hypium/index"
import { FrameHeader, ZstdFrameDecompressor, Long, ZstdFrameCompressor } from '@ohos/commons-compress';


export default function ZSTDCompressorTest() {
  let ARRAY_BYTE_BASE_OFFSET = Long.fromNumber(16)
  describe('ZSTDCompressorTest', function () {

    it('ZSTDTestMagic', 0, function (done) {
      testMagic()
      done()
    })


    it('ZSTDTestFrameHeader', 0, function (done) {
      testFrameHeader()
      done()
    })

  })

  function testMagic() {
    let buffer = new Int8Array(4);

    ZstdFrameCompressor.writeMagic(buffer, ARRAY_BYTE_BASE_OFFSET, ARRAY_BYTE_BASE_OFFSET.add(buffer.length));
    ZstdFrameDecompressor.verifyMagic(buffer, ARRAY_BYTE_BASE_OFFSET, ARRAY_BYTE_BASE_OFFSET.add(buffer.length));
  }

  function testFrameHeader() {
    verifyFrameHeader(1, 1024, new FrameHeader(Long.fromNumber(2), -1, Long.fromNumber(1), Long.fromNumber(-1), true));
    verifyFrameHeader(256, 1024, new FrameHeader(Long.fromNumber(3), -1, Long.fromNumber(256), Long.fromNumber(-1), true));

    verifyFrameHeader(65536 + 256, 1024 + 128, new FrameHeader(Long.fromNumber(6), 1152, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 2, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 2, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 3, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 3, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 4, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 4, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 5, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 5, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 6, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 6, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 7, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 7, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));
    verifyFrameHeader(65536 + 256, 1024 + 128 * 8, new FrameHeader(Long.fromNumber(6), 1024 + 128 * 8, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));

    verifyFrameHeader(65536 + 256, 2048, new FrameHeader(Long.fromNumber(6), 2048, Long.fromNumber(65536 + 256), Long.fromNumber(-1), true));

    verifyFrameHeader(2147483647, 1024, new FrameHeader(Long.fromNumber(6), 1024, Long.fromString('2147483647'), Long.fromNumber(-1), true));
  }

  function verifyFrameHeader(inputSize: number, windowSize: number, expected: FrameHeader) {
    let buffer = new Int8Array(ZstdFrameCompressor.MAX_FRAME_HEADER_SIZE);

    let size: number = ZstdFrameCompressor.writeFrameHeader(buffer, ARRAY_BYTE_BASE_OFFSET, ARRAY_BYTE_BASE_OFFSET.add(buffer.length), inputSize, windowSize);

    expect(expected.headerSize.eq(size)).assertTrue();

    let actual: FrameHeader = ZstdFrameDecompressor.readFrameHeader(buffer, ARRAY_BYTE_BASE_OFFSET, ARRAY_BYTE_BASE_OFFSET.add(buffer.length));
    expect(actual.headerSize.eq(expected.headerSize)).assertTrue();
    expect(actual.contentSize.eq(expected.contentSize)).assertTrue();
    expect(actual.dictionaryId.eq(expected.dictionaryId)).assertTrue();
    expect(actual.hasChecksum == expected.hasChecksum).assertTrue();
    expect(actual.windowSize == expected.windowSize).assertTrue();
  }
}

